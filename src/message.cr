require "json"

struct Message
    include JSON::Serializable
    property name : String
    property message : String

    def initialize(@name : String, @message : String)

    end
end
