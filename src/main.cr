#!/bin/crystal run
require "http/server"

require "./session"

HOST = "0.0.0.0"
PORT = 3077
SESSIONS = {} of String => Session
RESOURCE_PATH = "res"
STATIC_PATH = "#{RESOURCE_PATH}/static"
BAD_REQUEST = 400
NOT_FOUND = 404

def fail(context : HTTP::Server::Context, code : Int32, message : String, page : Bool = true) : Void
    context.response.status_code = code
    context.response.status_message = message

    if page
        context.response.content_type = "text/html"
        context.response.print File.read("#{RESOURCE_PATH}/error.html").gsub("%error%", message)
    else
        context.response.content_type = "application/json"
        context.response.print({ "message" => message }.to_json)
    end
end

def is_valid_name(name : String) : Bool
    name.each_char do |c|
        return false if c != '-' && !c.ascii_lowercase?
    end

    return true
end

def handle_home(context : HTTP::Server::Context) : Void
    context.response.content_type = "text/html"
    context.response.print File.read("#{RESOURCE_PATH}/home.html")
end

def handle_session(context : HTTP::Server::Context) : Void
    context.response.content_type = "text/html"
    context.response.print File.read("#{RESOURCE_PATH}/session.html")   
end

def handle_join_session(context : HTTP::Server::Context) : Void
    name = context.request.query_params["name"]?

    if name.nil?
        fail context, BAD_REQUEST, "invalid request"
        return
    end

    if name == "host" || !is_valid_name name
        fail context, BAD_REQUEST, "invalid name"
        return
    end

    context.response.status_code = 302

    if SESSIONS.has_key? name
        context.response.headers["Location"] = "/session/#{name}"
    else
        session = Session.new name

        token = session.join "host"

        SESSIONS[name] = session

        context.response.headers["Location"] = "/session/#{name}?token=#{token}"
    end

    context.response.close
end

def handle_identify_self(context : HTTP::Server::Context) : Void
    session_id = context.request.query_params["session"]?
    name = context.request.query_params["name"]?

    if session_id.nil? || name.nil?
        fail context, BAD_REQUEST, "invalid request", false
        return
    end

    if !is_valid_name name
        fail context, BAD_REQUEST, "invalid name", false
        return
    end

    session = SESSIONS[session_id]?

    if session.nil?
        fail context, BAD_REQUEST, "session not found", false
        return
    end

    token = session.join name

    context.response.content_type = "application/json"
    context.response.print({ "token" => token }.to_json)
end

def handle_send_message(context : HTTP::Server::Context) : Void
    session_id = context.request.query_params["session"]?
    token = context.request.query_params["token"]?
    message = context.request.query_params["message"]?
    target = context.request.query_params["target"]?

    if session_id.nil? || token.nil? || message.nil?
        fail context, BAD_REQUEST, "invalid request"
        return
    end

    if message == ""
        fail context, BAD_REQUEST, "invalid request"
        return
    end

    session = SESSIONS[session_id]?

    if session.nil?
        fail context, BAD_REQUEST, "session not found"
        return
    end

    if target.nil?
        session.send token, message
    else
        session.send_to token, message, target
    end

    context.response.content_type = "application/json"
    context.response.print "{}"
end

def handle_poll_messages(context : HTTP::Server::Context) : Void
    session_id = context.request.query_params["session"]?
    token = context.request.query_params["token"]?

    if session_id.nil? || token.nil?
        fail context, BAD_REQUEST, "invalid request"
        return
    end

    session = SESSIONS[session_id]?

    if session.nil?
        fail context, BAD_REQUEST, "session not found"
        return
    end

    messages = session.poll_messages token

    context.response.content_type = "application/json"
    context.response.print({ "messages" => messages }.to_json)
end

def handle_poll_users(context : HTTP::Server::Context) : Void
    session_id = context.request.query_params["session"]?
    token = context.request.query_params["token"]?

    if session_id.nil? || token.nil?
        fail context, BAD_REQUEST, "invalid request"
        return
    end

    session = SESSIONS[session_id]?

    if session.nil?
        fail context, BAD_REQUEST, "session not found"
        return
    end

    users = session.poll_users token

    context.response.content_type = "application/json"
    context.response.print({ "users" => users }.to_json)
end

def handle_favicon(context : HTTP::Server::Context) : Void
    context.response.content_type = "image/x-icon"
    context.response.print File.read("#{RESOURCE_PATH}/favicon.ico")
end

def handle_static(context : HTTP::Server::Context) : Void
    prefix = "/static/"

    file_name = context.request.path[prefix.size, context.request.path.size - prefix.size]

    if file_name.includes? ".."
        fail context, BAD_REQUEST, "invalid request"
        return
    end

    path = "#{STATIC_PATH}/#{file_name}"

    if !File.exists? path
        fail context, NOT_FOUND, "file not found"
        return
    end

    content_type = ""

    if path.ends_with? ".js"
        content_type = "application/ecmascript"
    elsif path.ends_with? ".css"
        content_type = "text/css"
    elsif path.ends_with? ".ttf"
        content_type = "font/ttf"
    elsif path.ends_with? ".png"
        content_type = "image/png"
    else
        fail context, BAD_REQUEST, "invalid request"
        return
    end

    context.response.content_type = content_type
    context.response.print File.read(path)
end

HANDLERS = {
    "/" => ->handle_home(HTTP::Server::Context),
    "/session/.*" => ->handle_session(HTTP::Server::Context),
    "/join-session" => ->handle_join_session(HTTP::Server::Context),
    "/identify-self" => ->handle_identify_self(HTTP::Server::Context),
    "/send-message" => ->handle_send_message(HTTP::Server::Context),
    "/poll-messages" => ->handle_poll_messages(HTTP::Server::Context),
    "/poll-users" => ->handle_poll_users(HTTP::Server::Context),
    "/favicon.ico" => ->handle_favicon(HTTP::Server::Context),
    "/static/.*" => ->handle_static(HTTP::Server::Context)
}

server = HTTP::Server.new do |context|
    HANDLERS.each do |pattern, handler|
        if Regex.new("^#{pattern}$") =~ context.request.path
            handler.call context
        end
    end
end

puts "Server online at #{HOST}:#{PORT}"
server.listen HOST, PORT
