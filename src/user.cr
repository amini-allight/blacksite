require "./message"

class User
    getter name : String
    getter messages : Array(Message) = [] of Message
    getter last_seen : Int64

    def initialize(@name : String)
        @last_seen = Time.utc.to_unix
    end

    def send(name : String, message : String) : Void
         @messages << Message.new(name, message)       
    end

    def poll : Array(Message)
        @last_seen = Time.utc.to_unix
        messages = @messages

        @messages = [] of Message

        return messages
    end

    def time_since_seen : Int64
        Time.utc.to_unix - @last_seen
    end
end
