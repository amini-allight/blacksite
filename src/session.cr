require "file"
require "random"
require "openssl/hmac"

require "./user"

SECRET_KEY_SIZE = 32

class Session
    @name : String
    @secret_key : String
    @users : Hash(String, User) = {} of String => User

    def initialize(@name : String)
        data = Bytes.new SECRET_KEY_SIZE

        Random::Secure.random_bytes data
        @secret_key = data.hexdump
    end

    def join(name : String) : String
        @users.each do |token, user|
            return token if user.name == name
        end

        token = OpenSSL::HMAC.hexdigest OpenSSL::Algorithm::SHA256, @secret_key, name

        @users[token] = User.new name

        return token
    end

    def send(token : String, message : String) : Void
        user = @users[token]?

        return if user.nil?

        name = user.name

        save name, message

        @users.each do |token, user|
            next if user.name != "host" && user.name != name && name != "host"

            user.send name, message
        end
    end

    def send_to(token : String, message : String, target : String) : Void
        user = @users[token]?

        return if user.nil?

        name = user.name

        save "#{name} @ #{target}", message

        @users.each do |token, user|
            next if user.name != target && user.name != name

            user.send "#{name} @ #{target}", message
        end
    end

    def poll_messages(token : String) : Array(Message)
        user = @users[token]?

        return [] of Message if user.nil?

        return user.poll
    end

    def poll_users(token : String) : Hash(String, Int64)
        users = {} of String => Int64

        user = @users[token]?

        return users if user.nil? || user.name != "host"

        @users.each do |token, user|
            next if user.name == "host"

            users[user.name] = user.time_since_seen
        end

        return users
    end

    def save(name : String, message : String) : Void
        text = File.exists?(file_name) ? File.read file_name : ""

        text += "> #{name}\n#{message}\n\n"

        File.write file_name, text
    end

    def file_name : String
        "session_#{@name}.txt"
    end
end
