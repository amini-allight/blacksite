function clearCookie(key)
{
    document.cookie = key + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}

function setCookie(key, value)
{
    clearCookie(key);

    document.cookie = key + "=" + value + "; Path=/; SameSite=strict";
}

function getCookie(key)
{
    var pairs = document.cookie.split(";");

    for (var i = 0; i < pairs.length; i++)
    {
        var pair = pairs[i].trim();

        var index = pair.indexOf("=");

        if (index < 0)
        {
            continue;
        }

        var pairKey = pair.substring(0, index);
        var pairValue = pair.substring(index + 1, pair.length);

        if (key == pairKey)
        {
            return pairValue;
        }
    }

    return null;
}

var lightTheme = false;

if (getCookie("lightTheme") == "1")
{
    toggleTheme();
}

function toggleTheme()
{
    if (lightTheme)
    {
        document.documentElement.style.cssText = "--fg: #ffffff; --bg: #000000";
        lightTheme = false;
        setCookie("lightTheme", "0");
    }
    else
    {
        document.documentElement.style.cssText = "--fg: #000000; --bg: #ffffff";
        lightTheme = true;
        setCookie("lightTheme", "1");
    }
}
