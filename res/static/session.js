const userTimeout = 5;

function getRequest(url, callback, errorCallback)
{
    var xmlHTTP = new XMLHttpRequest();

    xmlHTTP.onreadystatechange = function()
    {
        if (xmlHTTP.readyState != 4)
        {
            return;
        }

        var data = JSON.parse(xmlHTTP.responseText);

        if (xmlHTTP.status == 200)
        {
            callback(data);
        }
        else if (errorCallback)
        {
            errorCallback(data);
        }
    }
    xmlHTTP.open("GET", url, true);
    xmlHTTP.send(null);
}

function escapeHTML(unsafe)
{
    return unsafe.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;").replaceAll("'", "&#039;");
}

var host = (new URLSearchParams(window.location.search)).has("token");
var session = window.location.pathname.substring(9, window.location.pathname.length);
var token = "";
var target = null;
var previousUsers = {};

function onPollMessages(data)
{
    var container = document.getElementById("messages");

    for (var i = 0; i < data["messages"].length; i++)
    {
        var name = data["messages"][i]["name"];
        var message = data["messages"][i]["message"];

        var html = "<div class=\"message\" onclick=\"selectTarget(event, " + (container.children.length + i) + ", '" + name + "');\">";
        
        if (host)
        {
            html += "<span class=\"message-name\">" + name + "</span>";
        }

        html += "<p class=\"message-text\">" + escapeHTML(message) + "</p>";
        html += "</div>";

        container.innerHTML += html;
    }

    if (data["messages"].length != 0)
    {
        container.scrollTop = container.scrollHeight;
    }
}

function onPollUsers(data)
{
    var users = {};

    for (var key in data["users"])
    {
        var value = data["users"][key];

        users[key] = value < userTimeout;
    }

    var changed = false;

    for (var key in users)
    {
        if (!previousUsers.hasOwnProperty(key))
        {
            changed = true;
            break;
        }

        if (users[key] != previousUsers[key])
        {
            changed = true;
            break;
        }
    }

    for (var key in previousUsers)
    {
        if (!users.hasOwnProperty(key))
        {
            changed = true;
            break;
        }
    }

    if (!changed)
    {
        return;
    }

    previousUsers = users;

    var container = document.getElementById("users");

    var html = "";

    for (var key in users)
    {
        var value = users[key];

        var className = "user-online";

        if (!value)
        {
            className = "user-offline";
        }

        html += "<div class=\"user\"><span class=\"user-name\" onclick=\"selectTarget(event, -1, '" + key + "');\">" + key + "</span><div class=\"" + className + "\"></div></div>";
    }

    container.innerHTML = html;
}

function onTick()
{
    getRequest("/poll-messages?session=" + session + "&token=" + token, onPollMessages);

    if (host)
    {
        getRequest("/poll-users?session=" + session + "&token=" + token, onPollUsers);
    }
}

function onIdentify(data)
{
    document.getElementById("cover").style.display = "none";

    token = data["token"];
}

function onIdentifyError(data)
{
    var message = data["message"];

    document.getElementById("error-message").textContent = "Error: " + message;
}

function identify(e)
{
    e.preventDefault();

    var name = document.getElementById("name").value;

    getRequest("/identify-self?session=" + session + "&name=" + name, onIdentify, onIdentifyError);
}

function send(e)
{
    if (e.keyCode != 13)
    {
        return;
    }

    e.preventDefault();

    var sendBox = document.getElementById("send-box");

    var message = sendBox.value;

    sendBox.value = "";

    getRequest("/send-message?session=" + session + "&token=" + token + "&message=" + message + (target != null ? "&target=" + target : ""), function(data){});
}

function selectTarget(e, index, name)
{
    if (!host)
    {
        return;
    }

    e.stopPropagation();

    var messages = document.getElementById("messages");
    var sendTarget = document.getElementById("send-target");

    for (var i = 0; i < messages.children.length; i++)
    {
        messages.children[i].style.borderColor = "var(--bg)";
    }

    if (name == "" || name == "host" || name.indexOf("@") >= 0)
    {
        target = null;
        sendTarget.textContent = "";
    }
    else
    {
        if (index >= 0)
        {
            messages.children[index].style.borderColor = "var(--fg)";
        }

        target = name;
        sendTarget.textContent = "@" + name;
    }
}

setInterval(onTick, 100);

if (host)
{
    onIdentify({ "token": (new URLSearchParams(window.location.search)).get("token") });
}
