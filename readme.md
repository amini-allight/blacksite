# Blacksite

An experiment in immersive text-based roleplaying. Named for its experimental nature and minimalist visual theme.

![](doc/icon.png)

![](doc/example.png)

## What?

This is an experimental platform for immersive text-based roleplaying. It is intended to be used to play something similar to a tabletop roleplaying game but in a manner that deliberately reduces the game master to a faceless stream of text and removes direct access to the other players. I built this to see how this impacts the game experience. I don't imagine this will be useful to terribly many people, but I'm releasing it just in case.

## Usage

This project is written in Crystal with no dependencies aside from the standard library. Run with `crystal run src/main.cr` or `./src/main.cr` and then visit `http://localhost:3077` to interact with the application.

- To start a session simply type a name into the provided field.
- Names must contain only lowercase letters and the `-` symbol.
- Players can join your session by typing the same session name, then a name for themselves.
- Players can then interact with the game master as though blacksite was a normal chat program.
- The game master can send a message to a specific user by clicking their name or one of these messages to target them.
- The game master can return to sending global messages by pressing escape.
- A player's messages can only be seen by the game master, not by other players.
- The theme can be switched from light to dark by clicking the square in the top left corner.

## Storage

The program records the content of each session into a text file named `session_SESSIONNAME.txt` in the directory in which it is run. These files are not read by the program and exist only for user reference.

## License

Developed by Amini Allight and licensed under the GPL 3.0 license. Includes files from the Open Sans font under the SIL Open Font License.
